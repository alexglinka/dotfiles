#!/usr/bin/env bash

# usage
# cat text.txt | ./nlp_tokenize.sh

#curl -s http://www.gutenberg.org/cache/epub/76/pg76.txt |
#cat text.txt |

NUM_WORDS="$1"


tr '[:upper:]' '[:lower:]' | 
grep -oE '\w+' | 
sort |
uniq -c | 
sort -nr | 
head -n $NUM_WORDS
