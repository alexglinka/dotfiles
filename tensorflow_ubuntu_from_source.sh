# TensorFlow from source
# Source: https://www.tensorflow.org/install/install_sources

# vagrant prep
# change memory to 2048 in Vagrant
# resize swap file to 2048
# NOTE: uncomment if needed
# sudo dd if=/dev/zero of=swapfile bs=1M count=2048

# download tensorflow source
git clone https://github.com/tensorflow/tensorflow 

# setup bazel
# source: https://bazel.build/versions/master/docs/install-ubuntu.html
# add Bazel distribution URI as a package source (one time setup)
echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list
curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install bazel -y
sudo apt-get upgrade bazel -y

# setup python 2.7 env
# source: https://www.tensorflow.org/install/install_sources
sudo apt-get install python-numpy python-dev python-pip python-wheel -y

# configure installation
cd tensorflow
# when prompted hit “enter” and answer “n”
./configure
bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
# check the .whl file name in directory: /tmp/tensorflow_pkg/
# use that file to install
sudo pip install /tmp/tensorflow_pkg/tensorflow-1.2.0rc2-cp27-cp27mu-linux_x86_64.whl
