# ---------------------------------
# mvn nodejs install & configuration
# ---------------------------------

# install dependencies
sudo apt-get update
sudo apt-get install build-essential libssl-dev -y


# install nvm: node-version manager
# https://github.com/creationix/nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.1/install.sh | bash

# reload bashrc
source ~/.bashrc
