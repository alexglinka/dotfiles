terraform {
	required_version = ">= 0.12"
}

provider "aws" {
	region  = "ca-central-1" 
}

resource "aws_security_group" "test_sg" {
  name = "test_sg"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outgoing traffic to anywhere.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  ami             = "ami-0d0eaed20348a3389"
	#instance_type   = "t2.nano"
  instance_type   = "t3a.xlarge"
  key_name        = var.key_name
	security_groups = [aws_security_group.test_sg.name]

	user_data = <<-EOF
		#!/bin/bash	
		sudo apt update				
		git clone https://alexglinka@bitbucket.org/alexglinka/dotvim.git ~/.vim
		cd ~/.vim/
		./submodules.sh
		curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o n
		sudo bash n 13.2.0			
		#sudo apt install redis-server -y
		EOF
}
