variable "server_port" {
	description	= "HTTP server port"
	type		= number
	default		= 80
}

variable "ssh_port" {
	description	= "SSH port"
	type		= number
	default		= 22 
}

variable "key_name" {
	type = string
	default = "hub"
}
