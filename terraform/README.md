SETUP INSTRUCTIONS

# env in ~/.profile
export AWS_ACCESS_KEY_ID={ACCESS_ID}
export AWS_SECRET_ACCESS_KEY={SECRET_KEY}

# install terraform
./install_terraform

# initiate tf directory
terraform init
