variable "server_port" {
	description	= "HTTP server port"
	type		= number
	default		= 8080
}

variable "elb_port" {
	description = "The port the ELB will use for HTTP requests"
	type = number
	default = 80
}
