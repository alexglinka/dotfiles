sudo apt-get install macchanger -y
echo 'if ! { [ "$TERM" = "screen" ] && [ -n "$TMUX" ]; } then' >> $HOME/.profile # prevents re-run in tmux or screen
echo "	sudo ifconfig wlan0 down" > macchanger >> $HOME/.profile 
echo "	sudo macchanger -r wlan0" >> macchanger >> $HOME/.profile 
echo "	sudo ifconfig wlan0 up" >> macchanger >> $HOME/.profile 
echo "fi" >> $HOME/.profile 
