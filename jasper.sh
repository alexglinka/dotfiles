#!/bin/bash

# ---------------------------------------
# based on raspberry pi install doc
# url: http://jasperproject.github.io/documentation/installation/
# ---------------------------------------

# ---------------------------------------
# install tools
# ---------------------------------------
sudo apt-get update
sudo apt-get install -y python-dev 
sudo apt-get install -y bison
sudo apt-get install -y libasound2-dev
sudo apt-get install -y libportaudio-dev
sudo apt-get install -y python-pyaudio

# workaround for issue w/ pip when installed via apt-get
# see: https://bugs.launchpad.net/ubuntu/+source/python-pip/+bug/1306991

cd /usr/local/src # requires sudo, cause perm issues w/ the next 2 lines
sudo get https://raw.github.com/pypa/pip/master/contrib/get-pip.py
sudo python get-pip.py


sudo pip install --upgrade setuptools


# ---------------------------------------
# add paths
# ---------------------------------------

# TODO check if exists in .profile
echo 'export LD_LIBRARY_PATH="/usr/local/lib"' >> $HOME/.profile

# ---------------------------------------
# install jasper
# ---------------------------------------

# TODO mkdir ~/projects if doesn't exists
# TODO evaluate if /usr/local/src is appropriate
cd ~/projects
git clone https://github.com/jasperproject/jasper-client.git jasper

# install python libraries required for jasper
sudo pip install -r jasper/client/requirements.txt 

# change mode to executable
chmod +x jasper/jasper.py

# install tts
sudo apt-get install -y espeak


