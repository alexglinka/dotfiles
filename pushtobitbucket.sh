# startbitbucket - creates remote bitbucket repo and adds it as git remote to cwd
# INCLUDE IN ~/.bashrc and use 'pushtobitbucket' command
function pushtobitbucket {
	username="alexglinka"
	password="Nextbigthing123"	
	current_dir_name=$(printf '%s\n' "${PWD##*/}")	
	
	curl --user $username:$password https://api.bitbucket.org/1.0/repositories/ --data name=$current_dir_name --data is_private='true'
	git init
	
	# craete .gitignore rules	
	touch .gitignore
	echo "node_modules/" >> .gitignore
	echo "bower_components/" >> .gitignore
	
	git remote add origin https://alexglinka@bitbucket.org/alexglinka/$current_dir_name
	git remote set-url origin https://$username:$password@bitbucket.org/alexglinka/$current_dir_name
	git add .
	git commit -m 'initial commit'
	git push -u origin master 
}

