# install solr
PROJECTS_DIR=~/projects
VER=5.5.0

wget http://apache.mirror.gtcomm.net/lucene/solr/$VER/solr-$VER.zip -P $PROJECTS_DIR 
unzip $PROJECTS_DIR/solr-$VER.zip -d $PROJECTS_DIR
rm $PROJECTS_DIR/solr-$VER.zip

# start solr
$PROJECTS_DIR/solr-$VER/bin/solr start -e cloud -noprompt

# stop solr
#$PROJECT_DIR/solr-5.5.0/bin/solr stop




