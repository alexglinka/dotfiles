# set up python env
apt-get update && apt-get upgrade
apt-get install python-pip python-dev python-virtualenv
pip install --upgrade pip

# install tensorflow
# versions: https://storage.googleapis.com/tensorflow/
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.12.1-cp27-none-linux_x86_64.whl
pip install --upgrade $TF_BINARY_URL

# test tensorflow basic
import tensorflow as tf
hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello)) # should print “'Hello, TensorFlow!”
a = tf.constant(10)
b = tf.constant(32)
print(sess.run(a + b)) # should print “42”

# test tensorflow image recognition
# https://www.tensorflow.org/tutorials/image_recognition
# ensure that models.git is in from the same branch as tensorflow 
# model: Inception-V3
# image: panda (hardcoded)
git clone https://github.com/tensorflow/models.git
cd models/tutorials/image/imagenet
python classify_image.py

# test tensorflow image recognition on custom image
# model: Inception-V3
# image: Norstrom (Off the Shoulder Dress)

wget http://g.nordstromimage.com/ImageGallery/store/product/Zoom/10/_12988830.jpg
python classify_image.py --image_file _12988830.jpg


