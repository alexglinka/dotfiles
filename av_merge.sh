# original video and audio must be: video.mp4 and audio.mp4
# consider changing to pass variables
# source: https://stackoverflow.com/questions/24804928/singler-line-ffmpeg-cmd-to-merge-video-audio-and-retain-both-audios

# step 1) extract audio from the video.mp4
ffmpeg -i video.mp4 1.mp3

# step 2) merge both audio.mp3 and 1.mp3
ffmpeg -i audio.mp3 -i 1.mp3  -filter_complex amerge -c:a libmp3lame -q:a 4 audiofinal.mp3

# step 3)remove the audio from video.mp4 (this step is not required. but just to do it properly)
ffmpeg -i video.mp4 -an videofinal.mp4

# step 4) now merge audiofinal.mp3 and videofinal.mp4
ffmpeg  -i videofinal.mp4 -i audiofinal.mp3 -shortest final.mp4

# in the latest version of ffmpeg it will only prompt u to use '-strict -2' in case it does then use this
#ffmpeg  -i videofinal.mp4 -i audiofinal.mp3 -shortest -strict -2 final.mp4

# cleanup
rm videofinal.mp4 audiofinal.mp3 1.mp3
