# sudo chown -R $USER:$USER /home/ubuntu/projects

mkdir -p $HOME/projects/data/db

echo "if ! ps aux | grep "[m]ongod" > /dev/null" >> $HOME/.bashrc
echo "then" >> $HOME/.bashrc
# echo "	sudo mongod --fork --nojournal --dbpath /home/ubuntu/projects/data/db --logappend --logpath /var/log/mongodb/mongodb.log" >> $HOME/.bashrc
echo "	sudo mongod --fork --nojournal --dbpath $HOME/projects/data/db --logappend --logpath /var/log/mongodb/mongodb.log" >> $HOME/.bashrc
echo "fi" >> $HOME/.bashrc

source $HOME/.bashrc


# zsh
sudo apt-get install zsh
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
chsh -s `which zsh`
# log out & log back in
