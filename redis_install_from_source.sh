sudo apt update
sudo apt install build-essential tcl -y
sudo apt install --reinstall make
cd /tmp
curl -O http://download.redis.io/redis-stable.tar.gz
tar xzvf redis-stable.tar.gz
cd redis-stable
sudo adduser --system --group --no-create-home redis
sudo mkdir /var/lib/redis
sudo chown redis:redis /var/lib/redis
sudo chmod 770 /var/lib/redis
make
make test
sudo make install
sudo mkdir /etc/redis
sudo cp /tmp/redis-stable/redis.conf /etc/redis

# start: implement step 3
# https://www.digitalocean.com/community/tutorials/how-to-install-redis-from-source-on-ubuntu-18-04
# end: implement step 3

sudo systemctl unmask  redis-server.service
sudo systemctl start redis

