# source: https://hub.docker.com/r/tensorflow/tensorflow/

docker pull tensorflow/tensorflow

# CPU only
docker run -it -p 8888:8888 tensorflow/tensorflow

# NVIDIA
# install nvidia-docker first: https://github.com/NVIDIA/nvidia-docker
nvidia-docker run -it -p 8888:8888 tensorflow/tensorflow:latest-gpu


