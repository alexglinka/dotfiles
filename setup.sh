#!/bin/bash

cd $HOME

# --------------------------------
# projects directory (incl. mongo)
# --------------------------------
mkdir -p $HOME/projects/data/db

# --------------------------------
# git configuration
# --------------------------------

git config --global user.email "alexglinka@gmail.com"
git config --global user.name "Alex Glinka"


# --------------------------------
# vim installation & configuration
# --------------------------------

# install vim
sudo apt-get install -y vim 

# configure .vim file
# TODO check if ~/.vim exists
git clone https://alexglinka@bitbucket.org/alexglinka/dotvim.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc

# download vim bundles
cd ~/.vim
git submodule init
git submodule update

# TODO issue w/ .vim/bundle/javascript plugin, won't install

# ---------------------------------
# tmux installation & configuration
# ---------------------------------

sudo apt-get install -y tmux

# TODO add .tmux.conf to .bashrc

# ---------------------------------
# UTILITIES 
# ---------------------------------

sudo apt-get install -y curl

# for apt-get-repository
sudo apt-get install -y software-properties-common

# cli browser
# cheat sheet: http://wiki.titan2x.com/index.php?title=W3m_cheat_sheet
sudo apt-get install -y w3m

# ---------------------------------
# NodeJS install & configuration
# ---------------------------------

# Install nvm: node-version manager
# https://github.com/creationix/nvm
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

# Load nvm 
source $HOME/.nvm/nvm.sh
# reload .bashrc
source $HOME/.bashrc
# install latest nodejs version 
nvm install stable 
# Modify $PATH to use <version>. Uses .nvmrc if available
nvm use stable 
# create .nvmrc telling which nodejs version nvm to use
echo "stable" > $HOME/.nvmrc

# --------------------------------
# install global node tools
# --------------------------------

# jshint, also used by vim's Syntastic
npm install -g jshint

# jscs javascript code style checker
# used with vim's syntastic
# see: http://jscs.info/overview.html
npm install -g jscs

# uglify-js for cli beautify
# usage: uglifyjs foo.js --beautify --output cutefoo.js
npm install -g uglify-js

# phonegap cordova
npm install -g phonegap
npm install -g cordova

# set jscs code style checker preset to "crockford"
# possible values: "airbnb", "crockford", "google", 
# "jquery", "mdcs", "wikimedia", "yandex"
# exclude rules reference:
# http://jscs.info/overview.html
# https://github.com/jscs-dev/node-jscs/blob/master/presets/crockford.json
echo { "preset": "crockford", "requireMultipleVarDecl": null } > ~/.jscsrc

npm install -g nodemon

# --------------------------------
# nginx install
# --------------------------------

sudo apt-get install -y nginx

# start nginx on login
echo "if /etc/init.d/nginx status | grep not > /dev/null" >> $HOME/.bashrc
echo "then" >> $HOME/.bashrc
echo "	sudo service nginx start" >> $HOME/.bashrc
echo "fi" >> $HOME/.bashrc

source $HOME/.bashrc

# make nginx/html the projects directory
cd /usr/share/nginx/html
sudo ln -s $HOME/projects projects

# ---------------------------------
# MongoDB install & configuration
# ---------------------------------
sudo apt-get install -y mongodb

# set .bashrc to check if mongo is running
# startup mongodb on login 
# w/ journal disabled to hd space
echo "if ! ps aux | grep "[m]ongod" > /dev/null" >> $HOME/.bashrc
echo "then" >> $HOME/.bashrc
echo "	sudo mongod --fork --nojournal --dbpath $HOME/projects/data/db --logappend --logpath /var/log/mongodb/mongodb.log" >> $HOME/.bashrc
echo "fi" >> $HOME/.bashrc

source $HOME/.bashrc


# TODO .inputrc >> $HOME/.bashrc

# -------------------------------
# vi key mappings in node repl
# see: https://nodejs.org/api/repl.html#repl_repl
# -------------------------------

sudo apt-get install -y rlwrap

# binds repl to rlwrap for vi commands
echo 'alias node="env NODE_NO_READLINE=1 rlwrap node"' >> $HOME/.bashrc

#----------------------------------
# TODO vim/repl integration 
# see: http://thlorenz.com/replpad/
# ---------------------------------

# ---------------------------------
# iptables configurations to allow
# crouton servers over network
# see: https://github.com/dnschneid/crouton/wiki/Running-servers-in-crouton
# TODO revise this as this is requesting sudo each login & tmux pane
# see: https://help.ubuntu.com/community/IptablesHowTo#Solution_.233_iptables-persistent
# see: http://www.microhowto.info/howto/make_the_configuration_of_iptables_persistent_on_debian.html
# TODO if not in bashrc then settings won't stick after reboot
# explore dotfiles running on startup
# ---------------------------------

sudo apt-get install -y iptables

echo "sudo /sbin/iptables -I INPUT -p tcp --dport 80 -j ACCEPT" >> $HOME/.bashrc
echo "sudo /sbin/iptables -I INPUT -p tcp --dport 3000 -j ACCEPT" >> $HOME/.bashrc
echo "sudo /sbin/iptables -I INPUT -p tcp --dport 8080 -j ACCEPT" >> $HOME/.bashrc

# ---------------------------------
# ssh client configs
# store pem files in .ssh
# chmod 400 (or 700) mykey.pem
# ---------------------------------
# TODO figure out to automate the setup
mkdir .ssh

# ---------------------------------
# Oracle Java
# ---------------------------------

sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update

# disable Oracle prompt
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
# may have to run this first
# sudo dpkg --configure -a
sudo apt-get install -y oracle-java7-installer


# ---------------------------------
# sound tools
# ---------------------------------

sudo apt-get install -y sox
# switch to ffmpeg which install libav-tools as well
sudo apt-get install -y libav-tools
