# startbitbucket - creates remote bitbucket repo and adds it as git remote to cwd
# INCLUDE IN ~/.bashrc and use 'pushtobitbucket' command
# usage: pushtobitbucket <username> <password>

function pushtobitbucket {
	username=$1
	password=$2
	
	current_dir_name=$(printf '%s\n' "${PWD##*/}")	
	
	curl --user $username:$password https://api.bitbucket.org/1.0/repositories/ \
	--data name=$current_dir_name \
	--data is_private='true'
	
	
	git init
	
	## craete .gitignore rules ##
	## uncomment next 2 lines - add echo for each ignore rule #
	# touch .gitignore
	# echo "node_modules/" >> .gitignore
	
	git remote add origin https://$username@bitbucket.org/$username/$current_dir_name
	git remote set-url origin https://$username:$password@bitbucket.org/$username/$current_dir_name
	git add .
	git commit -m 'initial commit'
	git push -u origin master 
}

