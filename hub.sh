# aws cli
sudo apt install unzip -y
curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
rm awscliv2.zip
sudo ./aws/install
alias aws="/usr/local/bin/aws2"

# terraform
wget https://releases.hashicorp.com/terraform/0.12.16/terraform_0.12.16_linux_amd64.zip
unzip terraform_0.12.16_linux_amd64.zip
rm terraform_0.12.16_linux_amd64.zip
sudo mv terraform /usr/local/bin
