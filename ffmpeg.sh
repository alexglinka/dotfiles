# yasm required for compilation
sudo apt-get install yasm

# download ffmeg source code
git clone git://source.ffmpeg.org/ffmpeg.git ffmpeg


# configure/build
cd ffmpeg
./configure
