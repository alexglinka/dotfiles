# Install nvm: node-version manager
# https://github.com/creationix/nvm
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

# Load nvm 
source $HOME/.nvm/nvm.sh
# reload .bashrc
source $HOME/.bashrc
# install latest nodejs version 
nvm install stable 
# Modify $PATH to use <version>. Uses .nvmrc if available
nvm use stable 
# create .nvmrc telling which nodejs version nvm to use
echo "stable" > $HOME/.nvmrc
# reload bashrc
source $HOME/.bashrc
