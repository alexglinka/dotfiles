# NOT TESTED

sudo apt-get update

# create source directory 
mkdir -p /usr/local/src/cmusphinx

# download, compile & install sphinxbase
cd /usr/local/src/cmusphinx
wget http://downloads.sourceforge.net/project/cmusphinx/sphinxbase/0.8/sphinxbase-0.8.tar.gz
tar -zxvf sphinxbase-0.8.tar.gz
cd ~/sphinxbase-0.8/
./configure --enable-fixed
make
sudo make install

# download, compile & install pocketsphinx
cd /usr/local/src/cmusphinx
wget http://downloads.sourceforge.net/project/cmusphinx/pocketsphinx/0.8/pocketsphinx-0.8.tar.gz
tar -zxvf pocketsphinx-0.8.tar.gz
cd ~/pocketsphinx-0.8/
./configure
make
sudo make install
