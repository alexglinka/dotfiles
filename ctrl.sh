# https://github.com/dnschneid/crouton/issues/811
sudo rm -f /var/lib/xkb/*.xkm

# put in ~/.bashrc
xmodmap -e 'remove mod4 = Super_L'
xmodmap -e 'keycode 133 = Control_L'
xmodmap -e 'add control = Control_L'
